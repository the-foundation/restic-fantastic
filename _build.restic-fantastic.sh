#/bin/bash

buildargs=""

startdir=$(pwd)
mkdir buildlogs
#/bin/bash -c "cd Hocker ;git pull origin master --recurse-submodules"
#cd Hocker/build/
sucess=true

#MODE=onefullimage
#MODE=allfeaturesincreasing
FILENAME=Dockerfile 

#IMAGETAG=$(date -u +%Y-%m-%d_%H.%M)"_"$FEATURESET"_"$CI_COMMIT_SHA;

buildfail=0

cleantags="";
tagstring="";
buildstring="";
          IMAGETAG=$(echo $FILENAME|sed 's/Dockerfile//g;s/^-//g' |awk '{print tolower($0)}')"_"$cleantags"_"$(date -u +%Y-%m-%d_%H.%M)"_"$(echo $CI_COMMIT_SHA|head -c8);
          IMAGETAG=$(echo "$IMAGETAG"|sed 's/_\+/_/g')
          start=$(date -u +%s)
          docker build -t hocker:$IMAGETAG $buildstring -f $FILENAME --rm=false . &> ${startdir}/buildlogs/build-${IMAGETAG}".log" 

      grep "^Successfully built " ${startdir}/buildlogs/build-${IMAGETAG}".log" || ( echo -e "\e[0m\e[3;40m" ; tail -n 15 ${startdir}/buildlogs/build-${IMAGETAG}".log"  ;echo -e "\e[0m" ;exit 100 )
          grep "^Successfully built " ${startdir}/buildlogs/build-${IMAGETAG}".log" || buildfail=100
          end=$(date -u +%s)
          seconds=$((end-start))
          echo -en "\e[1:42m"
          TZ=UTC printf "FINISHED: %d days %(%H hours %M minutes %S seconds)T\n" $((seconds/86400)) $seconds | tee -a ${startdir}/buildlogs/build-${IMAGETAG}".log" 
          echo -en "\e[0m"
        grep -q -e error -e Error -e failed -e Failed ${startdir}/buildlogs/build-${IMAGETAG}".log" && success=false

exit $success
