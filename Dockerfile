FROM ubuntu:bionic
## DANGER: not all ubuntu tags have /386 Arch 
RUN apt update && apt -y  install wget bzip2 curl socat trickle busybox p7zip-full  cpulimit python3-pip restic openssh-client  --no-install-recommends 
RUN apt update && apt -y  install python3-setuptools  --no-install-recommends  &&  bash -c 'curl https://rclone.org/install.sh |  bash &  pip3 install --upgrade pip ;pip3 install "cython<3.0.0" wheel || true  ; pip3 install "pyyaml<5.4.0" --no-build-isolation || true ; pip3 install aws-shell & wait'  && apt-get  remove -y python3-setuptools p7zip-full  && apt-get  -y autoremove && apt-get clean all && rm -rf /var/cache/apt/*cache*bin /var/cache/apt/archives/*.deb /usr/share/man/ || true
RUN echo "https://github.com"$(curl -s https://github.com$(curl -s "https://github.com/restic/restic/releases"|grep /tag/|grep href|sed 's/.\+href="//g'|cut -d'"' -f1|head -n1|sed 's~releases/tag/~releases/expanded_assets/~g' )|grep $(uname -m |sed 's/x86_64/amd64/g;s/aarch64/arm64/g;s/armhf/arm/g;s/armv7.\+/arm/g;s/armv6.\+/arm/g;s/armv8.\+/arm/g') |grep linux|cut -d'"' -f2|grep bz2) |tee /etc/.resticurl

# FIX the url 
RUN (uname -m |grep -q -e armv6 -e armv7 -e armv8 -e armhf && ( cat /etc/.resticurl |cut -d" " -f1 |cut -f1 > /tmp/.rurl;cat /tmp/.rurl > /etc/.resticurl )) || true 

## TRY TO DOWNLOAD CURRENT RESTIC (SOFT_FAIL TO APT VERSION)
RUN echo "MACHINE="$(uname -m)" URL="$(cat /etc/.resticurl) && cd /usr/bin && url=$(cat /etc/.resticurl ) && echo "getting $url" &&  (wget -O- "$url"|bunzip2 -d > restic || ( curl "$url"|bunzip2 -d > restic ) ) || true  &&   chmod +x restic || true 

RUN bash -c "uname -a;uname -m; (uname -a |grep armv6l  && cd /usr/bin && curl -s https://dl.min.io/client/mc/linux-arm6vl.tar.gz |tar xvz && chmod +x mc ) &  (uname -a |grep i386    && cd /usr/bin && curl -s https://dl.min.io/client/mc/linux-386.tar.gz    |tar xvz && chmod +x mc ) &  (uname -a |grep x86_64  && cd /usr/bin && wget https://dl.min.io/client/mc/release/linux-amd64/mc && chmod +x mc ) &  (uname -a |grep aarch64 && cd /usr/bin && wget https://dl.min.io/client/mc/release/linux-arm64/mc && chmod +x mc ) & (uname -a |grep armhf   && cd /usr/bin && wget https://dl.min.io/client/mc/release/linux-amd64/mc && chmod +x mc ) &  (uname -a |grep armv7   && cd /usr/bin && wget https://dl.min.io/client/mc/release/linux-amd64/mc && chmod +x mc ) & (uname -a |grep armv8   && cd /usr/bin && wget https://dl.min.io/client/mc/release/linux-amd64/mc && chmod +x mc ) & wait "
RUN which restic || apt-get -y --no-recommends install restic
RUN which restic


##FINALS START

COPY restic-fantastic.sh /bin/restic-fantastic.sh
COPY _test_restic_fantastic.sh /root/_test_restic_fantastic.sh
RUN bash /root/_test_restic_fantastic.sh
RUN chmod +x /bin/restic-fantastic.sh

# check disk usage
RUN du -h -d 1 -x / || true

# default command
CMD ["/bin/restic-fantastic.sh"]
ENTRYPOINT ["/bin/restic-fantastic.sh"]
